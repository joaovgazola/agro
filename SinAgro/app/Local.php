<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Local extends Model
{
    protected $fillable = [
        'cep', 'logradouro', 'numero', 'bairro', 'cidade', 'uf', 'referencia',
    ];
}
