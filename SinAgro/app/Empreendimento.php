<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empreendimento extends Model
{
    protected $fillable = [
      'nomeempre', 'atividade', 'valor', 'idLocal',
    ];
}
