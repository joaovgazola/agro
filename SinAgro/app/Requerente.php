<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requerente extends Model
{
    protected $fillable = [
        'nomeourazao', 'cpfoucnpj', 'idLocal', 'telefone', 'fax', 'email',
    ];
}
