<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Licenca extends Model
{
    protected $fillable = [
      'tipo', 'numerolicenca', 'ano', 'validade', 'idEmpreendimento',
    ];
}
