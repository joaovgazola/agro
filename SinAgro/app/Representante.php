<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Representante extends Model
{
    protected $fillable = [
        'nome', 'cargo', 'telefone', 'fax', 'celular', 'emailrepre', 'idLocal', 'autorizado', 'nomenao', 'cpfnao', 'telefonenao',
    ];
}
