<?php

namespace App\Http\Controllers;

use App\Dado;
use App\Requerente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()->tipo == 1){
            $dados = Dado::findOrFail(Auth::user()->idDados);
        }
        elseif (Auth::user()->tipo == 2){
            return redirect()->to(route('adminHome'));
        }
        $requerente = Requerente::findOrFail($dados->idRequerente);
        $cnpj = \session('cnpj');
        if($requerente->cpfoucnpj != $cnpj){
            Auth::user()->email->flash();
            $cnpj->flash();
            Auth::logout();
            Session::flash('erro', 'CPF ou CNPJ invalido');
        }
        return redirect()->to(route('home'));
    }
}
