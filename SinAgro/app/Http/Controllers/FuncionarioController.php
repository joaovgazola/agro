<?php

namespace App\Http\Controllers;

use App\Termo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class FuncionarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        if(Auth::user()->tipo == 1){
            return redirect()->back();
        }
        return view('prefeitura.home');
    }
    public function editTermo(){
        if(Auth::user()->tipo == 1){
            return redirect()->back();
        }
        $termo = Termo::firstOrCreate(['id' => '1']);
        return view('prefeitura.editTermo', ['termo'=>$termo]);
    }
    public function saveTermo(Request $request){
        $termo = Termo::FirstOrNew(['id' => '1']);
        $termo->delta = $request->hiddenArea;
        $termo->save();
        Session::flash('sucesso', 'Termo salvo com sucesso');
        return redirect()->to(route('editTermo'));
    }
}
