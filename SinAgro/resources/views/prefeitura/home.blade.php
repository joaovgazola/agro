@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Funcionário</div>

                    <div class="card-body">
                        Voce está logado como funcionario
                        <br>
                        <button class="btn btn-primary"  onclick="return window.location.href = '/admin/editTermo'" >Editar termos de requerimento</button>
                    </div>
                </div>
                <br>

            </div>
        </div>
    </div>
@endsection
