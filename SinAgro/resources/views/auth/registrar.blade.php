@extends('layouts.app')

@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://unpkg.com/imask"></script>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="container col-12">
                        <form>
                            <input type="hidden" name="tipos" id="tipos" value="1">
                               <div class="tab-content">
                                <div class="tab-pane active" id="tabb">
                                <div class="container">

                        <div data-spy="scroll" data-target="#navbar-example2" data-offset="0">

                          <div id="quill"></div>
                            <br>
                         <div class="progress">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4"
                                style="width: 16%;">
                                    Passo 1 
                                </div>
                            </div>
                         <br>
                            
                                <input type="checkbox" aria-label="Checkbox for following text input" required class="float-left mt-1">  Li e confirmo as informações dando ciência e conhecimento.
                                    
                                <a class="btn btn-primary btnNext float-right" style="color: #fff;">Próximo</a>

                            

                        </div>
                        
                    </div>

                            </div>
                            <div class="tab-pane" id="tab1">
                                <h2>
                                Requerimento Padrão
                                </h2>

                                <hr/>

                                <h4>
                                Dados do Requerente
                                </h4><br>

                           <div class="row">
                                <div class="col-lg-4">
                                    <strong>
                                        <label for="cpfoucnpj">CNPJ/CPF
                                    <strong style="color: red;">*</strong>
                                        </label>
                                    </strong>

                                    <input id="cpfoucnpj" type="text" name="cpfoucnpj" class="form-control" required>
                                
                                </div>

                                <div class="col-lg-8">
                                    <strong>
                                        <label for="nomeourazao">Razão Social/Pessoa Física
                                    <strong style="color: red;">*</strong>
                                        </label>
                                    </strong>
                                    <input type="text" name="nomeourazao" class="form-control">
                                
                                </div>

                            </div>
                            <br>

                            <div class="row"> 

                                <div class="col-lg-4">
                                    <strong>
                                        <label for="email">E-mail
                                    <strong style="color: red;">*</strong>
                                        </label>
                                    </strong>
                                    <input type="email" name="email" class="form-control"
                                     required>
                                </div>

                                <div class="col-lg-4">
                                    <strong>
                                        <label for="tel">Telefone
                                    <strong style="color: red;">*</strong>
                                        </label>
                                    </strong>
                                    <input type="text" name="telefone" class="form-control" placeholder="(00)00000-0000" 
                                        maxlength="14"
                                        onkeydown="javascript: fMasc( this, mTel );"
                                        required>

                                </div>

                                <div class="col-lg-4">
                                        <strong>
                                            <label for="">Outro Contato</label>
                                        </strong>
                                        
                                        <input type="text" name="fax" id="fax"
                                        class="form-control"
                                        placeholder="(00)00000-0000" 
                                        maxlength="14"
                                        onkeydown="javascript: fMasc( this, mTel );"><br>
                                     </div>
                            </div>
                            <br>
                            <div class="row">

                                <div class="col-lg-3">
                                    <strong>
                                        <label for="cep">CEP
                                    <strong style="color: red;">*</strong>
                                        </label>
                                    </strong>
                                    <input type="text" name="cep" id="cep"class="form-control"
                                    placeholder="00000-000"
                                    required
                                    maxlength="10"
                                    onkeydown="javascript: fMasc( this, mCEP );" required>

                                    <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank"> Não sei meu CEP </a><br>
                                </div>

                                <div class="col-lg-5">
                                    <strong>
                                        <label for="logradouro">Logradouro</label>
                                    </strong>
                                        <input type="text" name="logradouro" id="logradouro" class="form-control"  readonly required><br>

                                </div>

                                <div class="col-lg-3">
                                    <strong>
                                        <label for="cidade">Cidade</label>
                                    </strong>
                                    <input type="text" name="cidade" id="cidade" class="form-control" readonly required><br>
                                </div>

                                <div class="col-lg-1">
                                    <strong>
                                        <label for="uf">Estado</label>
                                    </strong>
                                    <input type="text" name="uf" id="uf" class="form-control"  readonly required><br>
                                </div>

                            </div>
                            
                            <div class="row"> 
                                <div class="col-lg-2">
                                    <strong>
                                        <label for="numero">Número
                                            <strong style="color: red;">*</strong>
                                        </label>
                                    </strong>

                                    <input type="number" name="numero" id="numero" class="form-control" min="1" required><br>
                                </div>

                                <div class="col-lg-4">
                                    <strong>
                                        <label for="bairro">Bairro</label>
                                    </strong>

                                    <input type="text" name="bairro" id="bairro" class="form-control"  readonly required><br>
                                </div>
                                <div class="col-lg-6">
                                    <strong>
                                        <label for="referencia">Referência</label>
                                    </strong>

                                    <input type="text" name="referencia" id="referencia" class="form-control">

                                </div>
                            </div>
                           
                            <hr/>
                        
                            <div class="progress">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4"
                                style="width:32%;">
                                    Passo 2
                                </div>
                            </div>

                            <br>

                        <a class="btn btn-primary btnPrevious " style="color: #fff;">Voltar</a>
                        <a class="btn btn-primary btnNext float-right" style="color: #fff;">Próximo</a>
    </div>
                            <div class="tab-pane" id="tab2">

                                <h4>
                                    Enviar Requerimento
                                </h4>
                                <hr/>
                                
                                <div class="table-responsive">
                                <table class="table table-hover table-bordered"> 
                                    <thead style="background-color: rgba(0,0,0,.03); text-align: center;">
                                        <tr>
                                          
                                         
                                          <th width="20%">
                                             
                                                 Selecionar
                                          </th>
                                          <th></th>
                                   
                                        </tr>
                                      </thead>
                           
                        
                                      <tbody style="text-align: center;">
                                     
                                        <tr>
                                          <td>
                                            <input type="radio" name="tipoRequerimento" id="tipoRequerimento" required>
                                          </td>
                                          <td width="95%">Análise de Projeto e Regitro de Estaelecimento</td>
                                        </tr>

                                        <tr>
                                          <td>
                                            <input type="radio"  name="tipoRequerimento" id="tipoRequerimento">
                                          </td>
                                          <td width="95%">Instalação do SIM - Serviço de Inspeção Municipal no Estabelecimento</td>
                                        </tr>

                                        <tr>
                                          <td>
                                            <input type="radio"  name="tipoRequerimento" id="tipoRequerimento">
                                          </td>
                                          <td width="95%">Licença para solicitação de ampliação de Estabelecimento</td>
                                        </tr>

                                        <tr>
                                          <td>
                                            <input type="radio"  name="tipoRequerimento" id="tipoRequerimento">
                                          </td>
                                          <td width="95%">Análise de Projeto e Regitro de Estaelecimento</td>
                                        </tr>
                                    
                                      </tbody>
                                    </table>
                                </div>
                                <br>
                                <hr/>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4"
                                    style="width: 48%;">
                                        Passo 3 
                                    </div>
                                </div>

                            <br>
                           
        
        <a class="btn btn-primary btnPrevious " style="color: #fff;">Voltar</a>
        <a class="btn btn-primary btnNext float-right" style="color: #fff;">Próximo</a>
    </div>
                <div class="tab-pane" id="tab3">
                    <h4>
                        Empreendimento
                    </h4>
                    <hr/>

                    <div class="row">
                        
                        <div class="col-lg-4">
                            <strong>
                                <label for="nomeempre">Nome do Empreendimento
                            <strong style="color: red;">*</strong>
                            </label>
                            </strong>

                            <input id="nomeempre" type="text" name="nomeempre" class="form-control" required>
                                
                        </div>

                        <div class="col-lg-4">
                            <strong>
                                <label for="atividade">Atividade
                            <strong style="color: red;">*</strong>
                            </label>
                            </strong>

                            <input id="atividade" type="text" name="atividade" class="form-control" required>
                                
                        </div>

                        <div class="col-lg-4">
                            <strong>
                                <label for="valor">Valor de Investimento
                            <strong style="color: red;">*</strong>
                            </label>
                            </strong>

                            <input id="valor" type="text" name="valor" class="form-control" required>
                                
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-12">
                      <strong>
                                <label for="valor">Descrição da Atividade
                            <strong style="color: red;">*</strong>
                            </label>
                            </strong>
                      <textarea class="form-control" rows="3" cols="50"
                        name="descricao" id="descricao" required>
                      </textarea>
                    </div>
                    </div>
                    <br>
                    <div class="row">

                                <div class="col-lg-3">
                                    <strong>
                                        <label for="cep">CEP
                                    <strong style="color: red;">*</strong>
                                        </label>
                                    </strong>
                                    <input type="text" name="cep" id="cep"class="form-control"
                                    placeholder="00000-000"
                                    maxlength="10"
                                    onkeydown="javascript: fMasc( this, mCEP );" required>

                                    <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank"> Não sei meu CEP </a><br>
                                </div>

                                <div class="col-lg-5">
                                    <strong>
                                        <label for="logradouro">Logradouro</label>
                                    </strong>
                                        <input type="text" name="logradouro" id="logradouro" class="form-control"  readonly required><br>

                                </div>

                                <div class="col-lg-3">
                                    <strong>
                                        <label for="cidade">Cidade</label>
                                    </strong>
                                    <input type="text" name="cidade" id="cidade" class="form-control" readonly required><br>
                                </div>

                                <div class="col-lg-1">
                                    <strong>
                                        <label for="uf">Estado</label>
                                    </strong>
                                    <input type="text" name="uf" id="uf" class="form-control"  readonly><br>
                                </div>

                            </div>
                            
                            <div class="row"> 
                                <div class="col-lg-2">
                                    <strong>
                                        <label for="numero">Número
                                            <strong style="color: red;">*</strong>
                                        </label>
                                    </strong>

                                    <input type="number" name="numero" id="numero" class="form-control" min="1" required><br>
                                </div>

                                <div class="col-lg-4">
                                    <strong>
                                        <label for="bairro">Bairro</label>
                                    </strong>

                                    <input type="text" name="bairro" id="bairro" class="form-control"  readonly required><br>
                                </div>
                                <div class="col-lg-6">
                                    <strong>
                                        <label for="referencia">Referência</label>
                                    </strong>

                                    <input type="text" name="referencia" id="referencia" class="form-control">

                                </div>
                            </div>
                           
                            <hr/>
                            <div class="progress">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4"
                                    style="width: 64%;">
                                        Passo 4 
                                </div>
                            </div>
                                <br>
                    <a class="btn btn-primary btnPrevious" style="color: #fff;" >Voltar</a>
                    <a class="btn btn-primary btnNext float-right" style="color: #fff;">Próximo</a>
                </div>

                <div class="tab-pane" id="tab4">
                    <h4>
                        O Empreendimento possui Licença ou Autorização anterior ?
                    </h4>
                    <hr/>

                    Não <input type="radio" onclick="javascript:yesnoCheck();"
                     name="yessno" id="noCheck" checked required>

                    Sim, especificar <input type="radio" onclick="javascript:yesnoCheck();"  name="yessno" id="yesCheck"> 
                    <br>
                    <div id="ifYes" style="visibility:hidden;">
                        <div class="panel-body">
                          <div class="row">
                              
                              <div class="col-lg-4">
                                    <strong>
                                        <label for="referencia">Tipo
                                         
                                     </label>
                                    </strong>
                                    <input type="text" name="tipo" id="tipo" class="form-control">

                              </div>

                              <div class="col-lg-2">
                                  
                                        <strong>
                                            <label for="numerolicenca">N° 
                                                
                                            </label>
                                        </strong>

                                        <input type="number" name="numerolicenca" id="numerolicenca" class="form-control" min="1">
                              </div>

                              <div class="col-lg-3">
                                    <strong>
                                        <label for="ano">Ano 
                                            
                                        </label>
                                    </strong>
                                    <input type="text" name="ano" id="ano" class="form-control" maxlength="4">

                              </div>

                              <div class="col-lg-3">
                                    <strong>
                                        <label for="ano">Validade 
                                           
                                        </label>
                                    </strong>
                                    <input type="date" name="validade" id="validade" class="form-control">

                              </div>

                            </div>
                        </div>  
                    </div>
                    <hr/>
                    <div class="progress">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4"
                        style="width: 80%;">
                        Passo 5 
                        </div>
                    </div>
                    <br>                   
                    <a class="btn btn-primary btnPrevious" style="color: #fff;" >Voltar</a>
                    <a class="btn btn-primary btnNext float-right" style="color: #fff;">Próximo</a>
                 </div>

                 <div class="tab-pane" id="tab5">
                    <h4>
                        Representante Legal para assuntos relacionados ao Requerimento
                    </h4>
                    <hr/>

                    <div class="row">
                        <div class="col-lg-4">
                             <strong>
                                    <label for="nome">Nome
                                     <strong style="color: red;">*</strong>
                                 </label>
                                </strong>
                                <input type="text" name="nome" id="nome" class="form-control" required>
                        </div>

                        <div class="col-lg-4">
                             <strong>
                                    <label for="cargo">Cargo
                                     <strong style="color: red;">*</strong>
                                 </label>
                                </strong>
                                <input type="text" name="cargo" id="cargo" class="form-control" required>
                        </div>

                        <div class="col-lg-4">
                             <strong>
                                    <label for="emailrepre">Email
                                     <strong style="color: red;">*</strong>
                                 </label>
                                </strong>
                                <input type="email" name="emailrepre" id="emailrepre" class="form-control" required>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-4">
                                    <strong>
                                        <label for="tel">Telefone
                                    <strong style="color: red;">*</strong>
                                        </label>
                                    </strong>
                                    <input type="text" name="telefone" class="form-control" placeholder="(00)0000-0000" 
                                        maxlength="14"
                                        onkeydown="javascript: fMasc( this, mTel );"
                                        required>

                                </div>

                                <div class="col-lg-4">
                                    <strong>
                                        <label for="tel">Celular
                                    <strong style="color: red;">*</strong>
                                        </label>
                                    </strong>
                                    <input type="text" name="celular" class="form-control"
                                     placeholder="(00)00000-0000" maxlength="14"
                                     onkeydown="javascript: fMasc( this, mTel );"
                                     required>

                                </div>

                                <div class="col-lg-4">
                                        <strong>
                                            <label for="">Outro Contato</label>
                                        </strong>
                                        
                                        <input type="text" name="fax" id="fax"
                                        class="form-control"
                                        placeholder="(00)0000-0000" 
                                        maxlength="14"
                                        onkeydown="javascript: fMasc( this, mTel );"><br>
                                     </div>
                    </div>
                    <br>
                    <div class="row">

                                <div class="col-lg-3">
                                    <strong>
                                        <label for="cep">CEP
                                    <strong style="color: red;">*</strong>
                                        </label>
                                    </strong>
                                    <input type="text" name="cep" id="cep"class="form-control"
                                    placeholder="00000-000"
                                    maxlength="10"
                                    onkeydown="javascript: fMasc( this, mCEP );" required>

                                    <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank"> Não sei meu CEP </a><br>
                                </div>

                                <div class="col-lg-5">
                                    <strong>
                                        <label for="logradouro">Logradouro</label>
                                    </strong>
                                        <input type="text" name="logradouro" id="logradouro" class="form-control"  readonly required><br>

                                </div>

                                <div class="col-lg-3">
                                    <strong>
                                        <label for="cidade">Cidade</label>
                                    </strong>
                                    <input type="text" name="cidade" id="cidade" class="form-control" readonly required><br>
                                </div>

                                <div class="col-lg-1">
                                    <strong>
                                        <label for="uf">Estado</label>
                                    </strong>
                                    <input type="text" name="uf" id="uf" class="form-control"  readonly required><br>
                                </div>

                            </div>
                            
                            <div class="row"> 
                                <div class="col-lg-2">
                                    <strong>
                                        <label for="numero">Número
                                            <strong style="color: red;">*</strong>
                                        </label>
                                    </strong>

                                    <input type="number" name="numero" id="numero" class="form-control" min="1" required><br>
                                </div>

                                <div class="col-lg-4">
                                    <strong>
                                        <label for="bairro">Bairro</label>
                                    </strong>

                                    <input type="text" name="bairro" id="bairro" class="form-control"  readonly required><br>
                                </div>
                                <div class="col-lg-6">
                                    <strong>
                                        <label for="referencia">Referência</label>
                                    </strong>

                                    <input type="text" name="referencia" id="referencia" class="form-control">

                                </div>

                                <div class="col-lg-12">
                                    <p>Está autorizado a receber o documento solicitado ?</p>
                                </div>
                                <div class="col-lg-12">
                                    Sim <input type="radio" onclick="javascript:check();" name="yesno" id="yes" checked required>

                                   Não <input type="radio" onclick="javascript:check();"  name="yesno" id="no" > 

                                    <div id="iffYes" style="visibility:hidden;">
                                        <br>
                                        <div class="panel-body">
                                            <div class="row">

                                                <div class="col-lg-4">
                                                    <strong>
                                                        <label for="nomenao">Nome
                                                    <strong style="color: red;">*</strong>
                                                        </label>
                                                    </strong>

                                                    <input id="nomenao" type="text" name="nomenao" class="form-control" required>
                                                
                                                </div>

                                                <div class="col-lg-4">
                                                    <strong>
                                                        <label for="cpfnao">CPF
                                                    <strong style="color: red;">*</strong>
                                                        </label>
                                                    </strong>

                                                    <input id="cpfnao" type="text" name="cpfnao" class="form-control"
                                                    onkeydown="javascript: fMasc( this, mCPF );"
                                                    maxlength="14" placeholder="000.000.000-00" 
                                                     required>
                                                
                                                </div>

                                                 <div class="col-lg-4">
                                                    <strong>
                                                        <label for="tel">Telefone
                                                    <strong style="color: red;">*</strong>
                                                        </label>
                                                    </strong>
                                                    <input type="text" name="telefonenao" class="form-control" id="telefonenao" 
                                                     placeholder="(00)0000-0000" maxlength="13"
                                                     onkeydown="javascript: fMasc( this, mTel );"
                                                     required>

                                                </div>

                                                
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                
                            </div>
                    <hr/>
                    <div class="progress">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4"
                        style="width: 100%;">
                        Passo 6 
                        </div>
                    </div>
                    <br>
                    <a class="btn btn-primary btnPrevious" style="color: #fff;" >Voltar</a>
                    <input type="submit" name="salvar" class="btn btn-primary float-right" value="Salvar"><br>
                 </div>
</div> 

                        </form>     
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<ul class="nav nav-tabs" style="visibility: hidden;">
     <li class="nav-item active"><a href="#tabb" class="nav-link" data-toggle="tab" ></a></li>
    <li class="nav-item"><a href="#tab1" class="nav-link" data-toggle="tab" ></a></li>
    <li class="nav-item"><a href="#tab2" class="nav-link"data-toggle="tab"></a></li>
    <li class="nav-item"><a href="#tab3" class="nav-link" data-toggle="tab"></a></li>
    <li class="nav-item"><a href="#tab4" class="nav-link" data-toggle="tab"></a></li>
    <li class="nav-item"><a href="#tab5" class="nav-link" data-toggle="tab"></a></li>
</ul>
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>

<script>

    var quill = new Quill('#quill', {
        theme: 'snow',
        readOnly: true,
        modules: {
            toolbar: false
        }

    });
    <?php
    if(!empty($termo->delta)){
        echo 'quill.setContents('.$termo->delta.')';
    }
    ?>
</script>

<script type="text/javascript">
    
var maskCpfOuCnpj = IMask(document.getElementById('cpfoucnpj'), {
            mask:[
                {
                    mask: '000.000.000-00',
                    maxLength: 11
                },
                {
                    mask: '00.000.000/0000-00'
                }
            ]
        });

</script>


<script type="text/javascript">
     $('.btnNext').click(function(){
      $('.nav-tabs > .active').next('li').find('a').trigger('click');
    });

      $('.btnPrevious').click(function(){
      $('.nav-tabs > .active').prev('li').find('a').trigger('click');
    });
</script>

<script type="text/javascript">
    
    function yesnoCheck() {
    if (document.getElementById('yesCheck').checked) {
       document.getElementById('ifYes').style.visibility = 'visible';
    }
    else if(document.getElementById('noCheck').checked)
    {

        document.getElementById('ifYes').style.visibility = 'hidden';
        document.getElementById("tipo").value = "";
        document.getElementById("numerolicenca").value = "";
        document.getElementById("ano").value = "";
        document.getElementById("validade").value = "";
        

    }
}
    
</script>

<script type="text/javascript">
    // check 2
    function check(){
        if (document.getElementById('no').checked) {
            document.getElementById('iffYes').style.visibility = 'visible';
        }
        else if (document.getElementById('yes').checked)
        {
            document.getElementById('iffYes').style.visibility = 'hidden';
            document.getElementById("nomenao").value = "";
            document.getElementById("cpfnao").value = "";
            document.getElementById("telefonenao").value = "";
            
            
        }
    }
</script>
@endsection
