<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (){
  return view('welcome');
})->name('welcome');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/registrar', function (){
    if(\Illuminate\Support\Facades\Auth::guest()){
        $termo = \App\Termo::firstOrCreate(['id'=>'1']);
        return view('auth.registrar', ['termo'=>$termo]);
    }
    else{
        return redirect()->back();
    }
})->name('registrar');
Route::get('/admin/home', 'FuncionarioController@index')->name('adminHome');

Route::get('/admin/login', function (){
    if(\Illuminate\Support\Facades\Auth::guest()){
        return view('prefeitura.login');
    }
    elseif(\Illuminate\Support\Facades\Auth::user()->tipo == 2){
        return redirect()->to(route('adminHome'));
    }
    else{
        return redirect()->back();
    }
})->name('adminLogin');
Route::get('/admin/register', function (){
    if(\Illuminate\Support\Facades\Auth::guest()){
        return view('prefeitura.register');
    }
    elseif(\Illuminate\Support\Facades\Auth::user()->tipo == 2){
        return redirect()->to(route('adminHome'));
    }
    else{
        return redirect()->back();
    }
})->name('adminRegister');
Route::any('/admin/editTermo', 'FuncionarioController@editTermo')->name('editTermo');
Route::any('/admin/saveTermo', 'FuncionarioController@saveTermo')->name('saveTermo');