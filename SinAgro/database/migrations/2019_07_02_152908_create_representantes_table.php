<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepresentantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('representantes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome');
            $table->string('cargo');
            $table->string('telefone');
            $table->string('fax');
            $table->string('celular');
            $table->string('emailrepre');
            $table->unsignedBigInteger('idLocal');
            $table->foreign('idLocal')->references('id')->on('locals')->onDelete('cascade');
            $table->tinyInteger('autorizado');
            $table->string('nomenao')->nullable();
            $table->string('cpfnao')->nullable();
            $table->string('telefonenao')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('representantes');
    }
}
