<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dados', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('idRequerente');
            $table->tinyInteger('tipoRequerimento');
            $table->unsignedBigInteger('idEmpreendimento');
            $table->longText('descricao');
            $table->unsignedBigInteger('idRepresentante');
            $table->foreign('idRequerente')->references('id')->on('requerentes');
            $table->foreign('idEmpreendimento')->references('id')->on('empreendimentos');
            $table->foreign('idRepresentante')->references('id')->on('representantes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dados');
    }
}
