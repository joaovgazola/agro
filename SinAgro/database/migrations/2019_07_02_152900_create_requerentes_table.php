<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequerentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requerentes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nomeourazao');
            $table->string('cpfoucnpj');
            $table->unsignedBigInteger('idLocal');
            $table->foreign('idLocal')->references('id')->on('locals')->onDelete('cascade');
            $table->string('telefone');
            $table->string('fax');
            $table->string('email');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requerentes');
    }
}
